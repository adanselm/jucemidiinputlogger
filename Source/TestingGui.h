/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.3.2

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2017 - ROLI Ltd.

  ==============================================================================
*/

#pragma once

//[Headers]     -- You can add your own extra header files here --
#include "../JuceLibraryCode/JuceHeader.h"
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
                                                                    //[/Comments]
*/
class TestingGui  : public Component,
                    public ListBoxModel,
                    public AsyncUpdater
{
public:
    //==============================================================================
    TestingGui ();
    ~TestingGui();

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.

    // ListBoxModel ifc:
    int getNumRows() override;
    void paintListBoxItem(int rowNumber, Graphics& g, int width, int height,
                          bool rowIsSelected) override;

    void putLine(const String& line);
    void handleAsyncUpdate() override;
    //[/UserMethods]

    void paint (Graphics& g) override;
    void resized() override;



private:
    //[UserVariables]   -- You can add your own custom variables in this section.

    StringArray mText;
    //[/UserVariables]

    //==============================================================================
    std::unique_ptr<ListBox> mList;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TestingGui)
};

//[EndFile] You can add extra defines here...
//[/EndFile]
