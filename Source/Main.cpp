/*
 ==============================================================================
 ==============================================================================
 */
#include "../JuceLibraryCode/JuceHeader.h"
#include "TestingGui.h"

//==============================================================================
class JuceMidiInputLoggerApplication  : public JUCEApplication, public MidiInputCallback
{
public:
  //==============================================================================
  JuceMidiInputLoggerApplication() {}
  
  const String getApplicationName() override       { return ProjectInfo::projectName; }
  const String getApplicationVersion() override    { return ProjectInfo::versionString; }
  bool moreThanOneInstanceAllowed() override       { return true; }
  
  //==============================================================================
  void initialise (const String& commandLine) override
  {
    mainWindow.reset (new MainWindow (getApplicationName()));
    midiInput.reset(MidiInput::createNewDevice("JuceMidiInput", this));
    midiInput->start();
  }
  
  void shutdown() override
  {
    mainWindow = nullptr; // (deletes our window)
    midiInput->stop();
    midiInput = nullptr;
  }
  
  //==============================================================================
  void handleIncomingMidiMessage(MidiInput* source, const MidiMessage& message) override
  {
    if(mainWindow)
      mainWindow->putLine(message.getDescription());
  }
  
  //==============================================================================
  void systemRequestedQuit() override
  {
    quit();
  }
  
  void anotherInstanceStarted (const String& commandLine) override
  {
  }
  
  //==============================================================================
  class MainWindow : public DocumentWindow
  {
  public:
    MainWindow (String name)  : DocumentWindow (name,
    Desktop::getInstance().getDefaultLookAndFeel().findColour(ResizableWindow::backgroundColourId),
                                                DocumentWindow::allButtons)
    {
      setUsingNativeTitleBar (true);
      gui.reset(new TestingGui());
      setContentNonOwned(gui.get(), true);
      
      centreWithSize (getWidth(), getHeight());
      setVisible (true);
    }
    
    void closeButtonPressed() override
    {
      JUCEApplication::getInstance()->systemRequestedQuit();
    }
    
    void putLine(const String& line)
    {
      if(gui)
        gui->putLine(line);
    }
    
  private:
    std::unique_ptr<TestingGui> gui;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainWindow)
  };
  
private:
  std::unique_ptr<MainWindow> mainWindow;
  std::unique_ptr<MidiInput> midiInput;
};

//==============================================================================
START_JUCE_APPLICATION (JuceMidiInputLoggerApplication)
