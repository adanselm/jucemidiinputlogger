/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.3.2

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2017 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
//[/Headers]

#include "TestingGui.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...

//[/MiscUserDefs]

//==============================================================================
TestingGui::TestingGui ()
{
    //[Constructor_pre] You can add your own custom stuff here..
    //[/Constructor_pre]

    mList.reset (new ListBox ("Input", this));
    addAndMakeVisible (mList.get());


    //[UserPreSize]
    mList->setColour(ListBox::backgroundColourId, Colours::white);
    putLine("Listening to port JuceMidiInput...");
    //[/UserPreSize]

    setSize (300, 600);


    //[Constructor] You can add your own custom stuff here..
    //[/Constructor]
}

TestingGui::~TestingGui()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    mList = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void TestingGui::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colours::white);

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void TestingGui::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    mList->setBounds (20, 20, getWidth() - 40, getHeight() - 40);
    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...

int TestingGui::getNumRows()
{
  return mText.size();
}

void TestingGui::paintListBoxItem(int rowNumber, Graphics& g,
                                  int width, int height, bool rowIsSelected)
{
  const String text = mText[rowNumber];
  g.drawText(text, 0, 0, width, height, Justification::centredLeft, true);
}

void TestingGui::putLine(const String& line)
{
  mText.add(line);
  triggerAsyncUpdate();
}

void TestingGui::handleAsyncUpdate()
{
  mList->updateContent();
  mList->scrollToEnsureRowIsOnscreen( mList->getModel()->getNumRows() - 1 );
}

//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="TestingGui" componentName=""
                 parentClasses="public Component, public ListBoxModel, public AsyncUpdater"
                 constructorParams="" variableInitialisers="" snapPixels="8" snapActive="1"
                 snapShown="1" overlayOpacity="0.330" fixedSize="0" initialWidth="300"
                 initialHeight="600">
  <BACKGROUND backgroundColour="ffffffff"/>
  <GENERICCOMPONENT name="" id="4f77fc988b9960bf" memberName="mList" virtualName=""
                    explicitFocusOrder="0" pos="20 20 40M 40M" class="ListBox" params="&quot;Input&quot;, this"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]
